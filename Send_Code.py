from dialIndicatorWrap import dialIndicator
from time import sleep
from datetime import datetime

import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SPREADSHEETID = "1hpwvTfKHD0qXz4UkDCdXNvqDZXVHCWTLWhVcOZIdVq4"
SCOPES = ["https://www.googleapis.com/auth/drive.file"]

if __name__ == "__main__":
    dial = dialIndicator(0, 2)


    # Setup sheet access

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_console()

        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Create new sheet

    now = datetime.now()
    title = now.strftime("%d/%m/%Y %H:%M:%S")

    spreadsheet = {
        'properties': {
            'title': "Indicator Reading " + title
        }
    }

    spreadsheet = service.spreadsheets().create(body=spreadsheet).execute()
    spreadsheet_id = spreadsheet.get('spreadsheetId')


    # Update values
    values = [0] * 7
    for x in range(0,60):
        for i in range(7):
            values[i] = dial.sample()
        if dial.getMode():
            unit = "Inch"
        else:
            unit = "mm"

        body = {
            'majorDimension' : "COLUMNS",
            'values' : [values, [unit]*7],
        }
        sheetRange = "Sheet1!A{}:B{}".format(x*7+1, x*7+7+1)

        sheet = service.spreadsheets()
        result = sheet.values().update(valueInputOption="RAW", spreadsheetId=spreadsheet_id, range=sheetRange, body=body).execute()