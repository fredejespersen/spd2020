#include <unistd.h>
#include <stdio.h>
#include <wiringPi.h>
#include <time.h>

using namespace std;

// #define CLK_PIN 0 // Gpio 17
// #define DATA_PIN 2 //Gpio 27

class dialIndicator{
public:
    dialIndicator(int clk, int data){
        wiringPiSetup(); // Initialize WiringPi
        piHiPri(99);
        clkPin = clk;
        dataPin = data;
        delayTime = 320;

        pinMode(clkPin, INPUT);
        pinMode(dataPin, INPUT);

        pullUpDnControl(clkPin, PUD_OFF);
        pullUpDnControl(dataPin, PUD_OFF);
    }

    double sample(void){
        // Wait for sync
        deltaClk = 0;
        sync = false;
        clock_gettime(CLOCK_MONOTONIC, &clkLast);
        while(!sync){
            // delayMicroseconds(1);
            if(digitalRead(clkPin)){
                clock_gettime(CLOCK_MONOTONIC, &clkCurrent);
                deltaClk = deltaClk + clkCurrent.tv_nsec - clkLast.tv_nsec;
                clkLast = clkCurrent;
                // printf("%u\n", deltaClk);
            }

            if(deltaClk > 50000000){ //50 mS
                // printf("SYNC at %u\n", deltaClk);
                sync = true;
                clkStatePrev = 1;
            }
        }
        

        index = 0;
        while(index < 24){
            clkState = digitalRead(clkPin);
            if(clkState != clkStatePrev){
                if(clkState == 0){
                    delayMicroseconds(this->delayTime);
                    data[23-index] = digitalRead(dataPin);
                    index++;
                }
                clkStatePrev = clkState;
            }
        }

        inch = bool(data[0]); // Inch indicator is 1st bit
        negative = bool(data[3]); // Negative indicator is 4th bit

        position = 0;
        for(int i=5; i<24; i++){
            position += double(data[i] << (23-i));
        }

        if(negative){
            position = -position;
        }

        if(inch){
            return position/2540;
        }
        else{
            return position/100;
        }
    }

    bool getMode(void){
        if(inch) return 1;
        else return 0;
    }

    void setDelay(int delay){
        this->delayTime = delay;
    }

private:
    struct timespec clkLast, clkCurrent;

    int delayTime;

    bool sync;
    unsigned int deltaClk;

    int index;
    int clkStatePrev, clkState;
    int data[24];

    bool inch;
    bool negative;
    double position;

    int clkPin, dataPin;
};

extern "C"
{
    dialIndicator* dialIndicator_new(int clkPin, int dataPin) {return new dialIndicator(clkPin, dataPin);}
    double dialIndicator_sample(dialIndicator* dial) {return dial->sample();}
    bool dialIndicator_getMode(dialIndicator* dial) {return dial->getMode();}
    void dialIndicator_setDelay(dialIndicator* dial, int delay) {dial->setDelay(delay);}
}

// Test main function

// int main(void){
//     // Setup pins
//     wiringPiSetup(); // Initialize WiringPi
//     piHiPri(99);

//     printf("Creating object\n");
//     dialIndicator dial(CLK_PIN, DATA_PIN);
//     printf("Object created\n");
    
//     while(true){
//         double sample = dial.sample();
//         if(dial.getMode()){
//             printf("%f inch\n", sample);
//         }
//         else{
//             printf("%f mm\n", sample);
//         }
//         usleep(250000);

//     }
//     return 0;
// }