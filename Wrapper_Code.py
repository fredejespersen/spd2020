import ctypes

lib = ctypes.cdll.LoadLibrary("./digitalDialClass.so")

class dialIndicator(object):
    def __init__(self, *args):
        lib.dialIndicator_new.argtypes = [ctypes.c_int, ctypes.c_int]
        lib.dialIndicator_new.restype = ctypes.c_void_p

        lib.dialIndicator_sample.argtypes = [ctypes.c_void_p]
        lib.dialIndicator_sample.restype = ctypes.c_double

        lib.dialIndicator_getMode.argtypes = [ctypes.c_void_p]
        lib.dialIndicator_getMode.restype = ctypes.c_bool

        lib.dialIndicator_setDelay.argtypes = [ctypes.c_int]
        lib.dialIndicator_setDelay.restype = ctypes.c_void_p

        self.obj = lib.dialIndicator_new(args[0], args[1])

    def sample(self):
        return lib.dialIndicator_sample(self.obj)

    def getMode(self):
        return lib.dialIndicator_getMode(self.obj)

    def setDelay(self, delay):
        lib.dialIndicator_setDelay(self.obj, delay)